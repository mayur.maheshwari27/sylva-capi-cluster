{{ define "Metal3MachineTemplateSpec-MD" }}
{{- $envAll := index . 0 -}}
{{- $machine_deployment_name := index . 1 -}}
{{- $machine_network_interfaces := index . 2 -}}
{{- $machine_capm3_specs := index . 3 -}}
nodeReuse: {{ pluck "nodeReuse" $envAll.Values.capm3 ($machine_capm3_specs | default dict) | last }}
template:
  spec:
    automatedCleaningMode: {{ pluck "automatedCleaningMode" $envAll.Values.capm3 ($machine_capm3_specs | default dict) | last }}
    dataTemplate:
      name: {{ $envAll.Values.name }}-md-metadata-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_network_interfaces $machine_capm3_specs.provisioning_pool_interface $machine_capm3_specs.primary_pool_interface $machine_capm3_specs.networkData | include "Metal3DataTemplateSpec-MD" | sha1sum | trunc 10 }}
    hostSelector: {{ $machine_capm3_specs.hostSelector | required "'hostSelector' needs to be defined under capm3, machine_deployment_defaults.capm3, or machine_deployments.xxxx.capmp3" | toYaml | nindent 6 }}
    image: {{ include "capm3-image" (tuple $envAll (printf ".capm3/.machine_deployment_default.capm3/.machine_deployments.%s.capm3" $machine_deployment_name) (mergeOverwrite (deepCopy $envAll.Values.capm3) $machine_capm3_specs)) | nindent 6 }}
{{ end }}


{{ define "Metal3DataTemplate-MD" }}
{{- $envAll := index . 0 -}}
{{- $machine_deployment_name := index . 1 -}}
{{- $machine_network_interfaces := index . 2 -}}
{{- $machine_provisioning_pool_interface := index . 3 -}}
{{- $machine_primary_pool_interface := index . 4 -}}
{{- $machine_networkData := (index . 5).networkData -}}
apiVersion: {{ $envAll.Values.apiVersions.Metal3DataTemplate }}
kind: Metal3DataTemplate
metadata:
  name: {{ $envAll.Values.name }}-md-metadata-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_network_interfaces $machine_provisioning_pool_interface $machine_primary_pool_interface $machine_networkData | include "Metal3DataTemplateSpec-MD" | sha1sum | trunc 10 }}
  namespace: {{ $envAll.Release.Namespace }}
spec:
# NOTE: the Metal3DataTemplate.spec fields .metaData & .networkData are immutable
# per https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/docs/api.md#updating-metadata-and-networkdata
{{ tuple $envAll $machine_network_interfaces $machine_provisioning_pool_interface $machine_primary_pool_interface $machine_networkData | include "Metal3DataTemplateSpec-MD" | indent 2 }}
{{ end }}


{{/*
abstract: |
  Returns the complete Metal3DataTemplate.spec.template.spec

values: |
  tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capm3.provisioning_pool_interface $machine_deployment_def.capm3.primary_pool_interface

usage: |
  In values.yaml:
    name: workload-cluster
    worker_machine_image: "Ubuntu 20.04 Pack"
    capm3:
      primary_pool_name: "primary-pool"
      primary_pool_network: 172.20.36.128
      primary_pool_gateway: "172.20.36.129"
      primary_pool_end: 172.20.36.150
      primary_pool_start: 172.20.36.140
      primary_pool_prefix: "26"
      provisioning_pool_name: "provisioning-pool"
      provisioning_pool_network: 172.20.39.192
      provisioning_pool_gateway: 172.20.39.193
      provisioning_pool_end: 172.20.39.219
      provisioning_pool_start: 172.20.39.213
      provisioning_pool_prefix: "26"
      dns_servers:
      - 10.193.21.160
    :
    machine_deployments:
      md0:
        :
        network_interfaces:
          bond0:
            bond_mode: 802.3ad
            interfaces:
              - ens1fO
              - ens1f1
            vlans:
              - id: 92
          bond1:
            bond_mode: 802.3ad
            interfaces:
              - ens2fO
              - ens2f1
            vlans:
              - id: 92
          ens1f0:
            vlans:
              - id: 206
          ens1f1:
            vlans:
              - id: 206
          ens2f0:
            type: phy
            vlans:
              - id: 206
          ens2f1:
            type: phy
            vlans:
              - id: 206

  Inside XMachineTemplate-MD named template:
    {{ range $machine_deployment_name, $machine_deployment_def := $envAll.Values.machine_deployments }}
    {{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capm3.provisioning_pool_interface $machine_deployment_def.capm3.primary_pool_interface | include "Metal3DataTemplate-MD" }}
    apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
    kind: Metal3MachineTemplate
    metadata:
      name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | include "Metal3MachineTemplateSpec-remove-url-hostname" | sha1sum | trunc 10 }}
      namespace: {{ $envAll.Release.Namespace }}
      labels:
        role: worker
    {{ include "sylva-capi-cluster.labels" $envAll | indent 4 }}
    spec:
      template:
        spec:
    {{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | indent 6 }}
    ---
    {{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3.provisioning_pool_interface $machine_deployment_def.capm3.primary_pool_interface | include "Metal3DataTemplate-MD" }}

  Inside Metal3MachineTemplateSpec-MD named template:
    {{- $envAll := index . 0 -}}
    {{- $machine_deployment_name := index . 1 -}}
    {{- $machine_network_interfaces := index . 2 -}}
    {{- $machine_provisioning_pool_interface := index . 3 -}}
    {{- $machine_primary_pool_interface := index . 4 -}}
    {{- $machine_hostSelector := (index . 5).hostSelector -}}
    {{- $machine_networkData := (index . 5).networkData -}}
    dataTemplate:
      name: {{ $envAll.Values.name }}-md-metadata-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_network_interfaces $machine_provisioning_pool_interface $machine_primary_pool_interface $machine_networkData | include "Metal3DataTemplateSpec-MD" | sha1sum | trunc 10 }}

  Inside Metal3DataTemplate-MD named template:
    {{- $envAll := index . 0 -}}
    {{- $machine_deployment_name := index . 1 -}}
    {{- $machine_network_interfaces := index . 2 -}}
    {{- $machine_provisioning_pool_interface := index . 3 -}}
    {{- $machine_primary_pool_interface := index . 4 -}}
    apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
    kind: Metal3DataTemplate
    metadata:
      name: {{ $envAll.Values.name }}-md-metadata-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_network_interfaces $machine_provisioning_pool_interface $machine_primary_pool_interface $machine_networkData | include "Metal3DataTemplateSpec-MD" | sha1sum | trunc 10 }}
      namespace: {{ $envAll.Release.Namespace }}
    spec:
    {{ tuple $envAll $machine_network_interfaces $machine_provisioning_pool_interface $machine_primary_pool_interface $machine_networkData | include "Metal3DataTemplateSpec-MD" | indent 2 }}

return: |
  Renders the machine_deployments.X.network_interfaces for what's needed in CAPO only:
    apiVersion: infrastructure.cluster.x-k8s.io/v1beta1
    kind: Metal3DataTemplate
    metadata:
      name: workload-cluster-md-metadata-md0-23e71e0c5d
      namespace: {{ $envAll.Release.Namespace }}
    spec:
      clusterName: workload-cluster
      metaData:
        ipAddressesFromIPPool:
        - key: provisioningIP
          name: workload-cluster-provisioning-pool
        objectNames:
        - key: name
          object: machine
        - key: local-hostname
          object: machine
        - key: local_hostname
          object: machine
        prefixesFromIPPool:
        - key: provisioningCIDR
          name: workload-cluster-provisioning-pool
      networkData:
        links:
          ethernets:
          - id: ens1f0
            macAddress:
              fromHostInterface: ens1f0
            type: phy
          - id: ens1f1
            macAddress:
              fromHostInterface: ens1f1
            type: phy
          - id: ens2f0
            macAddress:
              fromHostInterface: ens2f0
            type: phy
          - id: ens2f1
            macAddress:
              fromHostInterface: ens2f1
            type: phy
          bonds:
          - id: bond0
            bondLinks:
              - ens1f0
              - ens1f1
            bondMode: 802.3ad
            macAddress: {}
          - id: bond1
            bondLinks:
              - ens2f0
              - ens2f1
            bondMode: 802.3ad
            macAddress: {}
          vlans:
          - id: bond0.13
            vlanID: 13
            vlanLink: bond0
            macAddress:
              fromHostInterface: ens1f0
        networks:
          ipv4:
          - id: bond0
            ipAddressFromIPPool: provisioning-pool
            link: bond0
            routes:
            - gateway:
                fromIPPool: provisioning-pool
              network: 172.20.39.192
              prefix: 26
          - id: bond0.13
            ipAddressFromIPPool: primary-pool
            link: bond0.13
            routes:
            - gateway:
                fromIPPool: primary-pool
              network: 0.0.0.0
        services:
          dns:
          - 10.193.21.160
*/}}
{{- define "Metal3DataTemplateSpec-MD" -}}
{{- $envAll := index . 0 -}}
{{- $machine_network_interfaces := index . 1 -}}
{{- $machine_provisioning_pool_interface := index . 2 -}}
{{- $machine_primary_pool_interface := index . 3 -}}
{{- $machine_networkData := index . 4 -}}
# https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/368
# TODO: try to templatize, get diff with https://github.com/metal3-io/cluster-api-provider-metal3/blob/6a9fe1d9e9fbea791094a709d5e79ad99c63b34b/examples/machinedeployment/machinedeployment.yaml#LL56C1-L136C18
clusterName: {{ $envAll.Values.name }}
metaData:
  ipAddressesFromIPPool:
  - kind: IPPool
    apiGroup: ipam.metal3.io
    name: {{ printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.provisioning_pool_name }}
    key: provisioningIP
  objectNames:
  - key: name
    object: machine
  - key: local-hostname
    object: machine
  - key: local_hostname
    object: machine
  prefixesFromIPPool:
  - kind: IPPool
    apiGroup: ipam.metal3.io
    name: {{ printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.provisioning_pool_name }}
    key: provisioningCIDR
  {{- if $envAll.Values.enable_longhorn }}
  fromLabels:
  - key: longhorn
    object: baremetalhost
    label: longhorn
  fromAnnotations:
  - key: sylva_longhorn_disks
    object: baremetalhost
    annotation: sylvaproject.org/default-longhorn-disks-config
  {{- end }}
networkData:
  links:
    ethernets:
    {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
    {{- if not (eq $machine_network_interface_def.type "bond") }}
    - id: {{ $machine_network_interface_name }}
      macAddress:
        fromHostInterface: {{ dig "macAddress" "fromHostInterface" $machine_network_interface_name $machine_network_interface_def }}
      type: {{ dig "type" "phy" $machine_network_interface_def  }}
    {{- end }}
    {{- end }}
    bonds:
    {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
    {{- if eq $machine_network_interface_def.type "bond" }}
    - id: {{ $machine_network_interface_name }}
      bondLinks:
        {{- range $bond_interfaces := $machine_network_interface_def.interfaces }}
        - {{ $bond_interfaces }}
        {{- end }}
      bondMode: {{ $machine_network_interface_def.bond_mode | default $envAll.Values.capm3.network_interfaces.bond_mode }}
      bondXmitHashPolicy: {{ $machine_network_interface_def.bondXmitHashPolicy | default $envAll.Values.capm3.network_interfaces.bondXmitHashPolicy }}
      macAddress:
       fromHostInterface: {{ first $machine_network_interface_def.interfaces }}
    {{- end }}
    {{- end }}
    vlans:
    {{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
    {{- if eq $machine_network_interface_def.type "bond" }}
{{ tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerBond" | indent 4}}
    {{- else }}
{{ tuple $ $machine_network_interface_name $machine_network_interface_def | include "listOfVlansPerInterface" | indent 4}}
    {{- end }}
    {{- end }}
  networks:
    ipv4:
    - id: {{ $machine_provisioning_pool_interface | default $envAll.Values.capm3.provisioning_pool_interface | required "provisioning_pool_interface missing for a machine deployment; needs to be specified under capm3, machine_deployment_default.capm3 or machine_deployments.xxx.capm3" }}
      ipAddressFromIPPool: {{ printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.provisioning_pool_name }}
      link: {{ $machine_provisioning_pool_interface | default $envAll.Values.capm3.provisioning_pool_interface | required "provisioning_pool_interface missing for a machine deployment; needs to be specified under capm3, machine_deployment_default.capm3 or machine_deployments.xxx.capm3" }}
      routes: {{ pluck "routes" ($envAll.Values.capm3.networkData.provisioning_pool_interface | default dict) ($machine_networkData.provisioning_pool_interface | default dict) | last | default list | toYaml | nindent 8 }}
    - id: {{ $machine_primary_pool_interface | default $envAll.Values.capm3.primary_pool_interface | required "primary_pool_interface missing for a machine deployment; needs to be specified under capm3, machine_deployment_default.capm3 or machine_deployments.xxx.capm3" }}
      ipAddressFromIPPool: {{ printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.primary_pool_name }}
      link: {{ $machine_primary_pool_interface | default $envAll.Values.capm3.primary_pool_interface | required "primary_pool_interface missing for a machine deployment; needs to be specified under capm3, machine_deployment_default.capm3 or machine_deployments.xxx.capm3" }}
      {{- if or $envAll.Values.capm3.networkData.primary_pool_interface $machine_networkData.primary_pool_interface }}
      routes:
{{ pluck "routes" $envAll.Values.capm3.networkData.primary_pool_interface $machine_networkData.primary_pool_interface | last | toYaml | indent 8 }}
      {{- else }}
      routes:
      - gateway:
          fromIPPool: {{ printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.primary_pool_name }}
        network: 0.0.0.0
      {{- end }}
  services:
    dns: {{ $envAll.Values.capm3.dns_servers | toYaml | nindent 4 }}
{{- end -}}
