{{- define "capd-RKE2ControlPlaneSpec" }}
registrationMethod: internal-first
agentConfig:
  # TODO: test a rke2-capd deployment with "cis_profile: cis-1.23" to see what breaks
  # excluding from rke2-capd for now
  cisProfile: null
  kubelet:
    extraArgs:
        eviction-hard: "nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%"
  ntp: null {{/* CAPD infra provider does not support NTP configuration for RKE2ControlPlane.spec.agentConfig */}}
serverConfig:
  tlsSan:
    - localhost
    - 127.0.0.1
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capo-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      provider-id: "openstack:///{{`{{ ds.meta_data.uuid }}`}}"
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  {{- if .Values.use_custom_rancher_dns_resolver }}
  - | {{ tuple "false" (include "rancher_dns_resolver" .) | include "shell-opensuse-dns" | nindent 4 }}
  {{- else }}
  - | {{ tuple "true" "_unused_" | include "shell-opensuse-dns" | nindent 4 }}
  {{- end }}
files: []
  {{/* workaround for the fact that CABPR v0.2.0 RKE2ControlPlane.spec.agentConfig.nodeAnnotations does not result in node annotations for CAPO infra; see https://gitlab.com/sylva-projects/sylva-core/-/issues/417#note_1696524256 */}}
  {{- $cp_rke2_nodeAnnotations := mergeOverwrite (deepCopy .Values.rke2.nodeAnnotations) (.Values.control_plane.rke2.nodeAnnotations | default dict) -}}
  {{- if $cp_rke2_nodeAnnotations }}
postRKE2Commands:
    {{ range $node_annotation_key, $node_annotation_value := $cp_rke2_nodeAnnotations }}
  - /var/lib/rancher/rke2/bin/kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml annotate node $(hostname) {{ printf "%s=\"%s\"" $node_annotation_key $node_annotation_value }}
    {{- end -}}
  {{- end }}
{{- end }}

{{- define "capv-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      cloud-provider: external
serverConfig:
  disableComponents:
    kubernetesComponents:
      - "cloudController"
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capm3-RKE2ControlPlaneSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      provider-id: "metal3://{{`{{ ds.meta_data.providerid }}`}}"
  nodeName: {{`'{{ ds.meta_data.local_hostname }}'`}}
  {{- if .Values.enable_longhorn }}
  nodeLabels:
    node.longhorn.io/create-default-disk: "config"
  {{- end }}
preRKE2Commands:
  - |
    if command -v netplan >/dev/null; then
      netplan apply
    elif command -v wicked  >/dev/null; then
      wicked ifreload all
    else
      echo "No network backend found"
    fi
  - sleep 30 # fix to give OS time to become ready
  {{- if .Values.use_custom_rancher_dns_resolver }}
  - | {{ tuple "false" (include "rancher_dns_resolver" .) | include "shell-opensuse-dns" | nindent 4 }}
  {{- else }}
  - | {{ tuple "false" .Values.capm3.dns_servers | include "shell-opensuse-dns" | nindent 4 }}
  {{- end }}
  {{- if .Values.enable_longhorn }}
  - | {{ include "shell-longhorn-mounts" . | nindent 4 }}
  {{- end }}
files: []
postRKE2Commands: []
{{- end }}
