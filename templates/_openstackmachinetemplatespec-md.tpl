{{/*
abstract: |
  Returns the complete OpenStackMachineTemplate.spec.template.spec

values: |
  tuple $envAll $machine_deployment_def.network_interfaces

usage: |
  In values.yaml:
    name: workload-cluster
    worker_machine_image: "Ubuntu 20.04 Pack"
    capo:
      flavor_name: m1.large
      worker_flavor_name: m1.large
      ssh_key_name: sylva
      network_id: 489b1587-4aa4-49b2-be56-62af67efa68f
    machine_deployment_default:
      capo:
        server_group_id: foo
        security_group_name: capo-cluster-security-group-worker
    :
    machine_deployments:
      md0:
        :
        network_interfaces:
          secondary:
            network_id: c314d52c-80fe-42b6-9092-55be383d1951
            vnic_type: direct

  Inside XMachineTemplate-MD named template:
    {{- range $machine_deployment_name, $machine_deployment_def := $envAll.Values.machine_deployments }}
    apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
    kind: OpenStackMachineTemplate
    metadata:
      name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def | include "OpenStackMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
    spec:
      template:
        spec:
    {{ tuple $envAll $machine_deployment_name $machine_deployment_def | include "OpenStackMachineTemplateSpec-MD" | indent 6 }}


return: |
  Renders the machine_deployments.X.network_interfaces for what's needed in CAPO only:
    apiVersion: infrastructure.cluster.x-k8s.io/v1alpha5
    kind: OpenStackMachineTemplate
    metadata:
      name: workload-cluster-md-md0-b042ca2f6f
    :
    spec:
      template:
        spec:
          cloudName: capo_cloud
          flavor: m1.large
          identityRef:
            kind: Secret
            name: workload-cluster-capo-cloud-config
          image: Ubuntu 20.04 Pack
          sshKeyName: sylva
          serverGroupID: foo
          securityGroups:
            - name: default
            - name: capo-cluster-security-group-worker
          ports:
            - network:
                id: 489b1587-4aa4-49b2-be56-62af67efa68f
              description: primary
            :
            - network:
                id: c314d52c-80fe-42b6-9092-55be383d1951
              vnicType: direct
              description: secondary
*/}}
{{- define "OpenStackMachineTemplateSpec-MD" -}}
{{- $envAll := index . 0 -}}
{{- $machine_deployment_name := index . 1 -}}
{{- $machine_deployment_def := index . 2 -}}
{{- $machine_network_interfaces := $machine_deployment_def.network_interfaces -}}
{{- $machine_capo_specs := $machine_deployment_def.capo -}}
cloudName: capo_cloud
flavor: {{ pluck "flavor_name" $envAll.Values.capo $machine_capo_specs | last }}
identityRef:
  kind: Secret
  name: {{ $machine_capo_specs.identity_ref_secret.name }}
{{ include "capo-image" (tuple $envAll (printf ".capo/.machine_deployment_default.capo/.machine_deployments.%s.capo" $machine_deployment_name) (mergeOverwrite (deepCopy $envAll.Values.capo) (deepCopy $machine_capo_specs))) }}
sshKeyName: {{ $envAll.Values.capo.ssh_key_name }}
serverGroupID: {{ $machine_capo_specs.server_group_id | required "you need to set a Nova server group UUID in machine_deployment_defaults.capo.server_group_id or machine_deployment.xxxx.capo.server_group_id" }}
securityGroups:
{{- range $machine_capo_specs.security_group_names }}
  - name: {{ . }}
{{ end }}
ports:
  - network:
      id: {{ $machine_capo_specs.network_id | default $envAll.Values.capo.network_id }}
    description: primary
{{- range $machine_network_interface_name, $machine_network_interface_def := $machine_network_interfaces }}
{{- if $machine_network_interface_def.network_id }}
  - network:
      id: {{ $machine_network_interface_def.network_id }}
    vnicType: {{ $machine_network_interface_def.vnic_type }}
    description: {{ $machine_network_interface_name }}
{{- end }}
{{- end }}
{{- if (or $envAll.Values.capo.rootVolume $machine_capo_specs.rootVolume) }}
rootVolume:
  diskSize: {{ (mergeOverwrite $envAll.Values.capo.rootVolume ($machine_capo_specs.rootVolume | default dict)).diskSize | int }}
  volumeType: {{ (mergeOverwrite $envAll.Values.capo.rootVolume ($machine_capo_specs.rootVolume | default dict)).volumeType }}
{{- end }}
{{- end -}}
