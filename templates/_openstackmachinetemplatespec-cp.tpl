{{ define "OpenStackMachineTemplateSpec-CP" }}
cloudName: capo_cloud
flavor: {{ pluck "flavor_name" .Values.capo (.Values.control_plane.capo | default dict) | last }}
identityRef:
  kind: Secret
  name: {{ .Values.name }}-capo-cloud-config
{{ include "capo-image" (tuple . ".capo/.control_plane.capo" (mergeOverwrite (deepCopy .Values.capo) (deepCopy .Values.control_plane.capo))) }}
sshKeyName: {{ .Values.capo.ssh_key_name }}
serverGroupID: {{ .Values.control_plane.capo.server_group_id | required "you need to set a Nova server group UUID in control_plane.capo.server_group_id" }}
securityGroups:
{{- range .Values.control_plane.capo.security_group_names }}
  - name: {{ . }}
{{ end }}
ports:
  - network:
      id: {{ .Values.capo.network_id }}
    allowedAddressPairs:
    {{- if .Values.capo.extra }}
      {{- if .Values.capo.extra.allowed_address_pairs }}
        {{- range .Values.capo.extra.allowed_address_pairs }}
      - ipAddress: {{ .ip | quote }}
        {{- if .mac }}
        macAddress: {{ .mac | lower | quote }}
        {{- end -}}
        {{- end -}}
      {{- end }}
    {{- end }}
      - ipAddress: {{ .Values.cluster_virtual_ip }}
    {{- if .Values.bgp_lbs.metallb }}
      {{- range .Values.bgp_lbs.metallb.address_pools }}
        {{- range .addresses -}}
          {{- if regexMatch "^[0-9.]*-[0-9.]*$" . -}}
            {{- $range := split "-" . }}
            {{- $firstIPBits := split "." $range._0 -}}
            {{- $lastIPBits := split "." $range._1 }}
            {{- range untilStep ($firstIPBits._3 | int) ($lastIPBits._3 | add1 | int) 1 }}
      - ipAddress: {{ $firstIPBits._0 }}.{{ $firstIPBits._1 }}.{{ $firstIPBits._2 }}.{{ . }}
            {{- end -}}
          {{- else }}
            {{- if regexMatch "^[0-9.]*/[0-9.]*$" . }}
              {{- $range := split "/" . }}
              {{- $ip_add := $range._0 }}
              {{- if ne $ip_add $.Values.cluster_virtual_ip }}
      - ipAddress: {{ $ip_add }}
              {{- end }}
            {{- end }}
          {{- end }}
        {{- end }}
      {{- end }}
    {{- end }}
{{ if (or .Values.capo.rootVolume .Values.control_plane.capo.rootVolume) }}
rootVolume:
  diskSize: {{ (mergeOverwrite .Values.capo.rootVolume (.Values.control_plane.capo.rootVolume | default dict)).diskSize | int }}
  volumeType: {{ (mergeOverwrite .Values.capo.rootVolume (.Values.control_plane.capo.rootVolume | default dict)).volumeType }}
{{ end }}
{{ end }}
