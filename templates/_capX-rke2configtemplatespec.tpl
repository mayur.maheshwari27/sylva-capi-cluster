{{- define "capd-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs: {}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
files: []
{{- end }}

{{- define "capo-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      provider-id: "openstack:///{{`{{ ds.meta_data.uuid }}`}}"
  # TODO: confirm agentConfig.nodeName for CAPO
  # nodeName: {}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  - 'echo "node-ip: {{`"{{ ds.ec2_metadata.local_ipv4  }}"`}}" >> /etc/rancher/rke2/config.yaml'
  {{- if .Values.use_custom_rancher_dns_resolver }}
  - | {{ tuple "false" (include "rancher_dns_resolver" .) | include "shell-opensuse-dns" | nindent 4 }}
  {{- else }}
  - | {{ tuple "true" "_unused_" | include "shell-opensuse-dns" | nindent 4 }}
  {{- end }}
files: []
{{- end }}

{{- define "capv-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      cloud-provider: "external"
  nodeName: {{`"{{ ds.meta_data.hostname }}"`}}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  - sleep 30 # fix to give OS time to become ready, per https://github.com/rancher-sandbox/cluster-api-provider-rke2/blob/main/samples/vmware/rke2configtemplate.yaml
files: []
{{- end }}

{{- define "capm3-RKE2ConfigTemplateSpec" }}
agentConfig:
  kubelet:
    extraArgs:
      provider-id: "metal3://{{`{{ ds.meta_data.providerid }}`}}"
  nodeName: {{`"{{ ds.meta_data.local_hostname }}"`}}
  {{- if .Values.enable_longhorn }}
  nodeLabels:
    node.longhorn.io/create-default-disk: "config"
  {{- end }}
preRKE2Commands:
  - echo "Preparing RKE2 bootstrap" > /var/log/my-custom-file.log
  - |
    if command -v netplan >/dev/null; then
      netplan apply
    elif command -v wicked  >/dev/null; then
      wicked ifreload all
    else
      echo "No network backend found"
    fi
  - sleep 30 # fix to give OS time to become ready, per https://github.com/rancher-sandbox/cluster-api-provider-rke2/blob/main/samples/metal3/sample-cluster.yaml
  {{- if .Values.use_custom_rancher_dns_resolver }}
  - | {{ tuple "false" (include "rancher_dns_resolver" .) | include "shell-opensuse-dns" | nindent 4 }}
  {{- else }}
  - | {{ tuple "false" .Values.capm3.dns_servers | include "shell-opensuse-dns" | nindent 4 }}
  {{- end }}
  {{- if .Values.enable_longhorn }}
  - | {{ include "shell-longhorn-mounts" . | nindent 4 }}
  {{- end }}
files: []
postRKE2Commands: []
{{- end }}
