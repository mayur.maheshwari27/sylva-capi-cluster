{{- define "KubeadmConfigTemplateSpec" -}}
  {{- $envAll := index . 0 -}}
  {{- $machine_deployment_def := index . 1 -}}

  {{- $machine_kubelet_extra_args := $machine_deployment_def.kubelet_extra_args -}}
  {{- $machine_kubeadm := $machine_deployment_def.kubeadm -}}
  {{- $machine_additional_commands := $machine_deployment_def.additional_commands -}}
  {{- $machine_additional_files := $machine_deployment_def.additional_files -}}
  {{- $machine_infra_provider := $machine_deployment_def.infra_provider -}}

  {{/*********** Initialize the components of the KubeadmConfigTemplate.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_kubelet_extra_args $machine_additional_files $machine_infra_provider | include "base-KubeadmConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-KubeadmConfigTemplateSpec" $machine_infra_provider) $envAll | fromYaml }}

joinConfiguration: {{ mergeOverwrite $base.joinConfiguration $infra.joinConfiguration | toYaml | nindent 2 }}
{{- if $envAll.Values.ntp }}
ntp: {{ mergeOverwrite $base.ntp $infra.ntp | toYaml | nindent 2 }}
{{- end }}
{{- $md_additional_commands := deepCopy ($envAll.Values.additional_commands | default dict) -}}
{{- if $machine_additional_commands }}
  {{- tuple $md_additional_commands $machine_additional_commands | include "merge-append" }}
{{- end }}
preKubeadmCommands:
  {{ $infra.preKubeadmCommands | toYaml | nindent 2 }}
  {{ $base.preKubeadmCommands | toYaml | nindent 2 }}
{{- if $md_additional_commands.pre_bootstrap_commands }}
  {{ $md_additional_commands.pre_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}
files: {{ concat $base.files $infra.files | default list | toYaml | nindent 2 }}
users: {{ mergeOverwrite (deepCopy ($envAll.Values.kubeadm | default dict)) $machine_kubeadm | dig "users" list | toYaml | nindent 2 }}
  {{- $base_merged_infra := dict }}
  {{- tuple $base_merged_infra $base $infra | include "merge-append" }}
postKubeadmCommands:
  {{ $base_merged_infra.postKubeadmCommands | toYaml | nindent 2 }}
{{- if $md_additional_commands.post_bootstrap_commands }}
  {{ $md_additional_commands.post_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}
{{- end }}
