{{ define "MachineDeploymentSpec" }}
{{- $envAll := index . 0 -}}
{{- $machine_deployment_def := index . 1 -}}
{{- $config_template_resource_kind := index . 2 -}}
{{- $config_template_resource_name := index . 3 -}}
{{- $machine_deployment_name := index . 4 -}}
clusterName: {{ $envAll.Values.name }}
replicas: {{ $machine_deployment_def.replicas }}
selector:
  matchLabels:
    cluster.x-k8s.io/cluster-name: {{ $envAll.Values.name }}
template:
  metadata:
    labels:
      cluster.x-k8s.io/cluster-name: {{ $envAll.Values.name }}
  spec:
    version: {{ $envAll.Values.k8s_version }}
    clusterName: {{ $envAll.Values.name }}
{{ if and (eq $machine_deployment_def.infra_provider "capo") $machine_deployment_def.capo.failure_domain }}
    failureDomain: {{ $machine_deployment_def.capo.failure_domain }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
# TODO: check if CAPV could use MachineDeployment.spec.template.spec.failureDomain equal to VSphereDeploymentZone.infrastructure.cluster.x-k8s.io name
# since VSphereMachine does support it
# per https://doc.crds.dev/github.com/kubernetes-sigs/cluster-api-provider-vsphere/infrastructure.cluster.x-k8s.io/VSphereMachine/v1beta1@v1.6.1#spec-failureDomain
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
# TODO: check if CAPM3 does indeed use a MachineDeployment.spec.template.spec.failureDomain
# 23.08.23 seems to be still Open in https://github.com/metal3-io/cluster-api-provider-metal3/issues/402
{{ end }}
    bootstrap:
      configRef:
        apiVersion: {{ index $envAll.Values.apiVersions $config_template_resource_kind }}
        kind: {{ $config_template_resource_kind }}
        name: {{ $config_template_resource_name }}
    infrastructureRef:
{{ if eq $machine_deployment_def.infra_provider "capd" }}
        # https://github.com/kubernetes-sigs/cluster-api/blob/main/test/infrastructure/docker/examples/simple-cluster.yaml
      apiVersion: {{ $envAll.Values.apiVersions.DockerMachineTemplate }}
      kind: DockerMachineTemplate
      name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ include "DockerMachineTemplateSpec" $envAll | sha1sum | trunc 10 }}
{{ else if eq $machine_deployment_def.infra_provider "capo" }}
      apiVersion: {{ $envAll.Values.apiVersions.OpenStackMachineTemplate }}
      kind: OpenStackMachineTemplate
      name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def | include "OpenStackMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
{{ else if eq $machine_deployment_def.infra_provider "capv" }}
      apiVersion: {{ $envAll.Values.apiVersions.VSphereMachineTemplate }}
      kind: VSphereMachineTemplate
      name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_def.network_interfaces $machine_deployment_def.capv | include "VSphereMachineTemplateSpec-MD" | sha1sum | trunc 10 }}
{{ else if eq $machine_deployment_def.infra_provider "capm3" }}
      apiVersion: {{ $envAll.Values.apiVersions.Metal3MachineTemplate }}
      kind: Metal3MachineTemplate
      name: {{ $envAll.Values.name }}-md-{{ $machine_deployment_name }}-{{ tuple $envAll $machine_deployment_name $machine_deployment_def.network_interfaces $machine_deployment_def.capm3 | include "Metal3MachineTemplateSpec-MD" | include "Metal3MachineTemplateSpec-remove-url-hostname" | sha1sum | trunc 10 }}
{{ end }}
{{ end }}
