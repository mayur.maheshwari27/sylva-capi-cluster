{{- define "RKE2ConfigTemplateSpec" }}
  {{- $envAll := index . 0 -}}
  {{- $machine_deployment_def := index . 1 -}}

  {{- $machine_kubelet_extra_args := $machine_deployment_def.kubelet_extra_args -}}
  {{- $machine_rke2_specs := $machine_deployment_def.rke2 -}}
  {{- $machine_additional_commands := $machine_deployment_def.additional_commands -}}
  {{- $machine_additional_files := $machine_deployment_def.additional_files -}}
  {{- $machine_node_class := $machine_deployment_def.node_class -}}
  {{- $machine_infra_provider := $machine_deployment_def.infra_provider -}}

  {{/*********** Initialize the components of the RKE2ConfigTemplate.spec.template.spec fields */}}
  {{- $base := tuple $envAll $machine_deployment_def| include "base-RKE2ConfigTemplateSpec" | fromYaml }}
  {{- $infra := include (printf "%s-RKE2ConfigTemplateSpec" $machine_infra_provider) $envAll | fromYaml }}

  {{/*********** Initialize agentConfig fields that are dict in value but should be list */}}

  {{- $kubelet_extra_args := mergeOverwrite (deepCopy ($base.agentConfig.kubelet.extraArgs| default dict)) (deepCopy ($infra.agentConfig.kubelet.extraArgs| default dict))  | include "transform_dict_to_list" | fromYamlArray }}
  
  {{- $node_labels := mergeOverwrite (deepCopy ($base.agentConfig.nodeLabels| default dict)) (deepCopy ($infra.agentConfig.nodeLabels | default dict))  | include "transform_dict_to_list" | fromYamlArray  }}
  {{- $node_taints := mergeOverwrite (deepCopy ($base.agentConfig.nodeTaints| default dict)) (deepCopy ($infra.agentConfig.nodeTaints| default dict))  | include "transform_dict_to_list" | fromYamlArray }}
  {{- $node_class_additional_commands := deepCopy (tuple $envAll "additional_commands" $machine_node_class | include "get_node_class_argument" | fromYaml) }}
  {{- $node_class_nodeAnnotations := deepCopy (tuple $envAll "nodeAnnotations" $machine_node_class | include "get_node_class_argument" | fromYaml) }}


  {{- $md_additional_commands := deepCopy ($envAll.Values.additional_commands | default dict) -}}
  {{- tuple $md_additional_commands $machine_additional_commands $node_class_additional_commands| include "merge-append" }}


agentConfig: {{ mergeOverwrite (omit (deepCopy $base.agentConfig) "kubelet" "nodeLabels" "nodeTaints") ( omit (deepCopy $infra.agentConfig) "kubelet" "nodeLabels" "nodeTaints") (dict "kubelet" (dict "extraArgs" $kubelet_extra_args)) (dict "nodeLabels" $node_labels) (dict "nodeTaints" $node_taints) | toYaml | nindent 2 }}
preRKE2Commands:
  {{ $infra.preRKE2Commands | toYaml | nindent 2 }}
  {{ $base.preRKE2Commands | toYaml | nindent 2 }}
{{- if $md_additional_commands.pre_bootstrap_commands }}
  {{ $md_additional_commands.pre_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}

files: {{ concat $base.files $infra.files | toYaml | nindent 2 }}

  {{- $md_rke2_nodeAnnotations := mergeOverwrite (deepCopy $envAll.Values.rke2.nodeAnnotations) (deepCopy ($machine_rke2_specs.nodeAnnotations | default dict)) $node_class_nodeAnnotations }}
  {{- $base_merged_infra := dict }}
  {{- tuple $base_merged_infra $base $infra | include "merge-append" }}
postRKE2Commands:
  {{ $base_merged_infra.postRKE2Commands | toYaml | nindent 2 }}
{{- if $md_additional_commands.post_bootstrap_commands }}
  {{ $md_additional_commands.post_bootstrap_commands | toYaml | nindent 2 }}
{{- end }}
{{/* workaround for the fact that CABPR v0.2.0 RKE2ConfigTemplate.spec.template.spec.agentConfig.nodeAnnotations does not result in node annotations; see https://gitlab.com/sylva-projects/sylva-core/-/issues/417#note_1668330146 */}}
{{- if $md_rke2_nodeAnnotations -}}
  {{ range $node_annotation_key, $node_annotation_value := $md_rke2_nodeAnnotations }}
  - /var/lib/rancher/rke2/bin/kubectl --kubeconfig /var/lib/rancher/rke2/agent/kubelet.kubeconfig annotate node $(hostname) {{ printf "%s=\"%s\"" $node_annotation_key $node_annotation_value }}
  {{- end -}}
{{- end }}
{{- end }}
