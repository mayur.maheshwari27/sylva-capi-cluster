{{- define "base-RKE2ConfigTemplateSpec" }}
{{- $envAll := index . 0 -}}

{{- $machine_deployment_def := index . 1 -}}

{{- $machine_kubelet_extra_args := $machine_deployment_def.kubelet_extra_args -}}
{{- $machine_rke2_additionalUserData := $machine_deployment_def.rke2.additionalUserData -}}
{{- $machine_rke2_nodeLabels := $machine_deployment_def.rke2.nodeLabels -}}
{{- $machine_rke2_nodeAnnotations := $machine_deployment_def.rke2.nodeAnnotations -}}
{{- $machine_additional_files := $machine_deployment_def.additional_files -}}
{{- $machine_node_class := $machine_deployment_def.node_class -}}
{{- $machine_taints := $machine_deployment_def.nodeTaints -}}
{{- $machine_infra_provider := $machine_deployment_def.infra_provider -}}

agentConfig:
{{- if $envAll.Values.cis_profile }}
  cisProfile: {{ $envAll.Values.cis_profile }}
{{- end }}
  additionalUserData:
    config: |{{ mergeOverwrite (deepCopy $envAll.Values.rke2.additionalUserData.config) (deepCopy $machine_rke2_additionalUserData.config) (dict "bootcmd" (list (tuple $envAll $machine_node_class | include "get_node_class_grub_command"))) | toYaml | nindent 6 }}
    strict: {{ pluck "strict" $envAll.Values.rke2.additionalUserData $machine_rke2_additionalUserData | last | default false | toYaml }}
  nodeLabels:
    {{  mergeOverwrite (deepCopy $envAll.Values.rke2.nodeLabels) (deepCopy ($machine_rke2_nodeLabels | default dict)) (deepCopy (tuple $envAll "nodeLabels" $machine_node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 4 }}
  {{/*
  CABPR v0.2.0 RKE2ConfigTemplate.spec.template.spec.agentConfig.nodeAnnotations does not result in node annotations; see https://gitlab.com/sylva-projects/sylva-core/-/issues/417#note_1668330146
  nodeAnnotations: {{ mergeOverwrite (deepCopy $envAll.Values.rke2.nodeAnnotations) ($machine_rke2_nodeAnnotations | default dict) | toYaml | nindent 4 }}
  */}}
  version: {{ $envAll.Values.k8s_version }}
  airGapped: {{ $envAll.Values.air_gapped }}
  nodeTaints: {{ mergeOverwrite (deepCopy ($envAll.Values.rke2.nodeTaints | default dict )) (deepCopy ($machine_taints | default dict)) (deepCopy (tuple $envAll "nodeTaints" $machine_node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 4 }}
  kubelet:
    extraArgs:
      {{ mergeOverwrite (deepCopy $envAll.Values.kubelet_extra_args) (dict "config" "/var/lib/rancher/rke2/server/kubelet-configuration-file.yaml") (deepCopy $machine_kubelet_extra_args) (deepCopy (tuple $envAll "kubelet_extra_args" $machine_node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 6}}

  {{- if $envAll.Values.ntp }}
  ntp:
{{ $envAll.Values.ntp | toYaml | indent 4 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
  {{- end }}
preRKE2Commands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  {{- if and (not (eq $machine_infra_provider "capd")) $envAll.Values.use_custom_rancher_dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if $envAll.Values.proxies.http_proxy }}
  - export HTTP_PROXY={{ $envAll.Values.proxies.http_proxy }}
  - export HTTPS_PROXY={{ $envAll.Values.proxies.https_proxy }}
  - export NO_PROXY={{ $envAll.Values.proxies.no_proxy }}
  {{- end }}
  {{- include "rke2-alias-commands" (tuple "md") | nindent 2 }}
files:
{{ $rke2ctfiles := list }}
{{ $rke2ctfiles = include "rke2_config_toml" $envAll | append $rke2ctfiles -}}
{{- if $envAll.Values.use_custom_rancher_dns_resolver }}
    {{- $rke2ctfiles = include "resolv_conf" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if ($envAll.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $rke2ctfiles = include "registry_mirrors" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- if $envAll.Values.proxies.http_proxy }}
    {{- $rke2ctfiles = include "rke2_agent_containerd_proxy" $envAll | append $rke2ctfiles -}}
{{- end }}
{{- $rke2ctfiles = include "rke2-kubelet-config-file" (tuple $envAll "kubelet_config_file_options" $machine_node_class | include "get_node_class_argument" | fromYaml) | append $rke2ctfiles -}}

{{- $additional_files := mergeOverwrite (deepCopy $envAll.Values.additional_files) $machine_additional_files }}
{{- if $additional_files }}
    {{- $rke2ctfiles = tuple $envAll $additional_files | include "additional_files" | append $rke2ctfiles -}}
{{- end }}
{{- if $rke2ctfiles -}}
    {{- range $rke2ctfiles -}}
        {{ . | indent 2 }}
    {{- end }}
{{- else }}
        []
{{- end }}
postRKE2Commands:
  - set -e
  {{- if $envAll.Values.enable_longhorn }}
  - | {{ tuple $machine_infra_provider "cabpr" "md" | include "shell-longhorn-node-metadata" | nindent 4 }}
  {{- end }}
{{- end }}
