{{- define "base-RKE2ControlPlaneSpec" }}

registrationMethod: address
registrationAddress: {{ .Values.cluster_virtual_ip }}
agentConfig:
{{- if .Values.cis_profile }}
  cisProfile: {{ .Values.cis_profile }}
{{- end }}
  additionalUserData:
    config: |{{ mergeOverwrite (deepCopy .Values.rke2.additionalUserData.config) (deepCopy .Values.control_plane.rke2.additionalUserData.config) (dict "bootcmd" (list (tuple . .Values.control_plane.node_class | include "get_node_class_grub_command"))) | toYaml | nindent 6 }}
    strict: {{ pluck "strict" .Values.rke2.additionalUserData .Values.control_plane.rke2.additionalUserData | last | default false | toYaml }}
  nodeLabels:
    {{ mergeOverwrite (deepCopy (.Values.rke2.nodeLabels | default dict )) (deepCopy (.Values.control_plane.rke2.nodeLabel | default dict)) (deepCopy (tuple . "nodeLabels" .Values.control_plane.node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 4 }}
  nodeAnnotations: {{ mergeOverwrite (deepCopy (.Values.rke2.nodeAnnotations | default dict )) (deepCopy (.Values.control_plane.rke2.nodeAnnotations | default dict)) (deepCopy (tuple . "nodeAnnotations" .Values.control_plane.node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 4 }}
  version: {{ .Values.k8s_version }}
  airGapped: {{ .Values.air_gapped }}
  nodeTaints: {{ mergeOverwrite (deepCopy (.Values.rke2.nodeTaints | default dict )) (deepCopy (.Values.control_plane.rke2.nodeTaints | default dict)) (deepCopy (tuple . "nodeTaints" .Values.control_plane.node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 4 }}
  kubelet:
    extraArgs:
      {{ mergeOverwrite (dict "config" "kubelet-configuration-file.yaml") (deepCopy (.Values.kubelet_extra_args | default dict )) (deepCopy (.Values.control_plane.kubelet_extra_args | default dict )) (deepCopy (tuple . "kubelet_extra_args" .Values.control_plane.node_class | include "get_node_class_argument" | fromYaml)) | toYaml | nindent 6 }}

  {{- if .Values.ntp }}
  ntp:
{{ .Values.ntp | toYaml | indent 4 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
  {{- end }}
serverConfig:
  cni: {{ .Values.default_cni }}
  disableComponents:
    pluginComponents:
      - rke2-ingress-nginx
  {{- if .Values.etcd }}
  etcd:
    customConfig:
      extraArgs:
      {{- range $key, $value := .Values.etcd }}
        - {{ $key }}={{ $value }}
      {{ end -}}
  {{- end }}
preRKE2Commands:
  - echo "fs.inotify.max_user_watches = 524288" >> /etc/sysctl.conf
  - echo "fs.inotify.max_user_instances = 512" >> /etc/sysctl.conf
  - sysctl --system
  {{- if and (not (eq .Values.capi_providers.infra_provider "capd")) .Values.use_custom_rancher_dns_resolver }}
  - systemctl restart systemd-resolved
  {{- end }}
  {{- if .Values.proxies.http_proxy }}
  - export HTTP_PROXY={{ .Values.proxies.http_proxy }}
  - export HTTPS_PROXY={{ .Values.proxies.https_proxy }}
  - export NO_PROXY={{ .Values.proxies.no_proxy }}
  {{- end }}
  {{- include "rke2-alias-commands" (tuple "cp") | nindent 2 }}
files:
{{ $rke2cpfiles := list }}
{{- $rke2cpfiles = include "rke2-kubelet-config-file" (tuple . "kubelet_config_file_options" .Values.control_plane.node_class | include "get_node_class_argument" | fromYaml) | append $rke2cpfiles -}}
{{- if .Values.use_custom_rancher_dns_resolver }}
    {{- $rke2cpfiles = include "resolv_conf" . | append $rke2cpfiles -}}
{{- end }}
{{- if (.Values.registry_mirrors | dig "hosts_config" "") }}
    {{- $rke2cpfiles = include "registry_mirrors" . | append $rke2cpfiles  -}}
{{- end }}
{{- if .Values.proxies.http_proxy }}
    {{- $rke2cpfiles = include "rke2_server_containerd_proxy" . | append $rke2cpfiles -}}
    {{- $rke2cpfiles = include "rke2_agent_containerd_proxy" . | append $rke2cpfiles -}}
{{- end }}
{{- if or (eq .Values.capi_providers.infra_provider "capo") (eq .Values.capi_providers.infra_provider "capv") (eq .Values.capi_providers.infra_provider "capm3") }}
    {{- $rke2cpfiles = include "kubernetes_rke2_metallb" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "kubernetes_rke2_metallb_l3" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "kubernetes_rke2_vip" . | append $rke2cpfiles  -}}
    {{- $rke2cpfiles = include "rke2_config_toml" . | append $rke2cpfiles  -}}
{{- if (eq .Values.default_cni "calico") }}
    {{- $rke2cpfiles = include "rke2_calico_helm_chart_config" . | append $rke2cpfiles -}}
{{- end }}
    {{- $rke2cpfiles = include "rke2_coredns_helm_chart_config" . | append $rke2cpfiles -}}
{{- end }}
{{- $additional_files := mergeOverwrite (deepCopy .Values.additional_files) (deepCopy .Values.control_plane.additional_files) }}
{{- if $additional_files }}
    {{- $rke2cpfiles = tuple . $additional_files | include "additional_files" | append $rke2cpfiles -}}
{{- end }}
{{- if .Values.audit_policies }}
    {{- $rke2cpfiles = include "audit_policy_config_file" . | append $rke2cpfiles -}}
{{- end }}
{{- if $rke2cpfiles -}}
{{- range $rke2cpfiles }}
{{ . | indent 2 }} {{/* this line needs to have no leading spaces to ensure correct rendering */}}
{{- end }}
{{- else }}
    []
{{- end }}
postRKE2Commands:
  - set -e
  {{- if .Values.enable_longhorn }}
  - | {{ tuple .Values.capi_providers.infra_provider "cabpr" "cp" | include "shell-longhorn-node-metadata" | nindent 4 }}
  {{- end }}
{{- end }}
